#ifndef __INPUT_HPP__
#define __INPUT_HPP__

#include <iostream>
#include <istream>

#include <string>

template<typename T> T input(std::istream& is, const std::string& prompt = ">>> "){
    T value;
    std::cout << prompt << std::flush;
    is >> value;
    return value;
}

template<typename T> T input(const std::string& prompt = ">>> "){
    return input<T>(std::cin, prompt);
}

template<typename T> std::istream& input(std::istream& is, T &value, const std::string& prompt = ">>> "){
    std::cout << prompt << std::flush;
	is >> value;
	return is;
}

template<typename T> std::istream& input(T &value, const std::string& prompt = ">>> "){
    return input<T>(std::cin, value, prompt);
}




template<typename T> std::istream& input_line(std::istream& is, T &value, const std::string& prompt = ">>> "){
    std::cout << prompt << std::flush;
	std::getline(is, value);
	return is;
}

template<typename T> std::istream& input_line(T &value, const std::string& prompt = ">>> "){
	return input_line<T>(std::cin, value, prompt);
}

template<typename T> T input_line(std::istream& is, const std::string& prompt = ">>> "){
	T value;
	std::cout << prompt << std::flush;
	std::getline(is, value);
	return value;
}

template<typename T> T input_line(const std::string& prompt = ">>> "){
	return input_line<T>(std::cin, prompt);
}




// ask for a yes or no answer
bool choice(const std::string& prompt = "y/n"){
    std::string value;
    std::cout << prompt << std::flush;
    std::cin >> value;
    return value == "y" || value == "Y" || value == "yes" || value == "Yes" || value == "YES";
}

//wait for enter to be pressed
void wait(){
	std::cin.ignore();
}

#endif